#!/bin/bash -ex
rm -rf aarch64
mkdir -p aarch64/usr/local/share/crystal/src/llvm/ext
make install stats=1 interpreter=1 DESTDIR=$PWD/aarch64
cd aarch64
tar cf crystal-aarch64-1.8.1.tar usr/
xz crystal-aarch64-1.8.1.tar
cd ..
mkdir -p build
mv aarch64/crystal-aarch64-1.8.1.tar.xz build/
