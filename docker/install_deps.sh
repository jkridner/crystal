#!/bin/bash -ex

if [ $(lsb_release -c -s) != "bullseye" ]; then
 echo "This script is setup for Debian 11"
 exit -1
fi

if grep -q docker /proc/1/cgroup; then
 echo "This script is running inside Docker"
elif [ $(id -u) != 0 ]; then
 echo "This script requires root permissions"
 sudo "$0" "$@"
 exit
fi

# Debian basics requirements
apt-get update
apt-get install -y build-essential curl gpg git autoconf wget tar libtool autotools-dev
apt-get install -y libssl-dev libxml2-dev libyaml-dev libgmp-dev libz-dev
sudo apt-get install -y \
  automake \
  build-essential \
  git \
  libbsd-dev \
  libedit-dev \
  libevent-dev \
  libgmp-dev \
  libgmpxx4ldbl \
  libpcre3-dev \
  libssl-dev \
  libtool \
  libxml2-dev \
  libyaml-dev \
  lld \
  llvm \
  llvm-dev\
  libz-dev

# My Arm build of Crystal
#wget https://debian.beagle.cc/images/crystal-aarch64-1.8.1.tar.xz
#tar xf crystal-aarch64-1.8.1.tar.xz -C /

# Link downloaded Crystal from cross-build environment to use for building Crystal
cc -fuse-ld=lld /usr/local/bin/crystal.o -o /usr/local/bin/crystal -rdynamic -static -L/usr/local/bin/../lib/crystal `llvm-config --libs --system-libs --ldflags --link-static 2> /dev/null` -lstdc++ -lpcre2-8 -lm -lgc -lpthread -levent -lrt -lpthread -ldl -lffi

if [ ! -e ./bdwgc ]; then
 git clone https://github.com/ivmai/bdwgc.git
 cd bdwgc/
 git clone https://github.com/ivmai/libatomic_ops.git
 autoreconf -vif
 ./configure --enable-static --disable-shared
 make -j
 make install
 cd ..
fi

