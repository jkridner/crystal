# Standalone Crystal

These are things I believe we need to make Crystal a stand-alone operating environment. The goal is
to make a computer that can be understood to its core and controlled by its owner.

## Interactive shell

There are several missing operations that would be required from the interactive shell.

- [ ] load - the ability to load a source file for editing
- [ ] save - the ability to save source currently editing
- [ ] # - the ability to insert lines into the current source file
- [ ] run - the ability to compile and execute the current source file
- [ ] build - the ability to generate an executable from the current source file

## Revision management

To preserve knowledge about the environment and programs

- [ ] save - this must preserve history to recall development and perfrom rollbacks
- [ ] validate - the ability to mark a program as valid such that automatic rollbacks are not done
  * This should be tied into the spec framework
- [ ] restore - the ability to rollback a program manually
- [ ] version - the ability to determine current version and version history

## Documentation

Crystal provides minimal introspection. To some extent, this is because it is a compiled language
with a documentation generator. The documentation must be explorable and be regenerated on updates.

- [ ] help - users must be provided an environment to help them understand the currently running environment
  * This should be tied into the command-line help system as well

## Command-line elimination

To avoid dependencies on additional command-line shells beyond the incorporated interactive shell should
be eliminated. All required functionality should be incorporated into the interactive shell.

Usage: crystal [command] [switches] [program file] [--] [arguments]

- [ ] init - generate a new project
  * This needs to be augmented by any installed framework.
- [ ] build - build an executable
- [ ] docs - generate documentation
- [ ] env - print Crystal environment information
- [ ] eval - eval code from args or standard input
- [ ] play - starts Crystal playground server
- [ ] run - build and run program
- [ ] spec - build and run specs (in spec directory)
- [ ] tool - run a tool
- [ ] help - show this help
- [ ] version - show version

## Bootstrap minimization

Rebuilding the interactive environment should not require going outside
the interactive environment.

This should eliminate the need for the Makefile as well as any external
library or tool dependencies.

- [ ] build compiler - the ability to rebuild the interactive environment
  * Must support cross-compilation to target new environments
- [ ] build samples - the ability to rebuild the sample programs
- [ ] run tests - the ability to run tests on the compiler and interactive environment
- [ ] version llvm - the ability to inspect the version of the compiler backend
- [ ] tool format - the ability to lint and format sources
- [ ] install - the ability to create a file system image of the compiler environment
  * bindir - the executable location
  * datadir - the shared source location
  * mandir - the help files location
  * destdir - the overall location
  * prefix - any local offset

## Shell elimination

There needs to be no reason to enter a shell.

- [ ] list - the ability to list the current source file
- [ ] list dir - the ability to examine the contents of directories
- [ ] make dir - the ability to create directories
- [ ] remove - the ability to remove files or directories
- [ ] rename - the ability to move files or directories
- [ ] \t - command-line completions
  * See /share/bash-completion/completions/crystal

## Play environment

This should become a first-class development environment that can be fully
edited within the compiler interactive environment.

- [ ] there should be access security
- [ ] there should be a file browser, editor and version explorer
- [ ] there should be access to the interactive environment

## Operating system, kernel and bootloader elimination

Eventually, we should be able to generate in Crystal the services needed from
an operating system, kernel or bootloader. We can take various approaches
to incrementally replace these.

- [ ] build Crystal for aarch64
- [ ] full static executable
  * We should make sure we aren't using any lurking dynamic libraries
- [ ] docker containter
  * There are a number of externally invoked exectuables outside of crystal to isolate and identify
- [ ] run as Linux init executable
  * This would eliminate any dependence on shell functions and other applications
  * The file system may or may not contain any dynamically linked libraries
  * Environment replication would need to duplicate or rebuild the bootloader and kernel
- [ ] run as Zephyr executable
  * This would eliminate multi-user and kernel/userspace divisions
- [ ] generate bootloader
