# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Contributor: Milan P. Stanić <mps@arvanta.net>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=crystal-unified
pkgver=1.13.3
pkgrel=0
_bootver=1.13.2
_llvmver=18
pkgdesc="The Crystal Programming Language"
url="https://crystal-lang.org/"
arch="x86_64 aarch64"
license="Apache-2.0"
depends="
	gc-dev
	gcc
	gmp-dev
	libatomic_ops
	libevent-dev
	libevent-static
	musl-dev
	pcre-dev
	pcre2-dev
	"
checkdepends="
	openssl-dev
	openssl-libs-static
	tzdata
	xz-static
	yaml-dev
	yaml-static
	zlib-dev
	"
makedepends="
	git
	crystal
	shards
	libxml2-dev
	llvm$_llvmver-dev
	"
[ "${BUILD_STATIC:-0}" -eq 1 ] && makedepends="$makedepends
	libxml2-static
	llvm$_llvmver-static
	zlib-static
	zstd-static
	"
subpackages="$pkgname-doc
	$pkgname-bash-completion
	$pkgname-zsh-completion
	"
source=""
srcdir="/builds/jkridner/crystal"
builddir="/builds/jkridner/crystal"

_coredir="/usr/lib/$pkgname/core"
_shardsdir="/usr/lib/$pkgname/shards"

export CRYSTAL_CONFIG_VERSION="$pkgver"
export CRYSTAL_CACHE_DIR="$srcdir/.cache"
export LLVM_CONFIG="/usr/lib/llvm$_llvmver/bin/llvm-config"

cleanup_srcdir() {
	echo "skipping cleanup of $srcdir"
}

prepare() {
	default_prepare

	echo "tmpdir: $tmpdir"
	echo "pkgdir: $pkgdir"
	echo "srcdir: $srcdir"
	echo "builddir: $builddir"

	# FIXME: These specs fail with invalid memory access.
	rm -f spec/compiler/compiler_spec.cr

	# FIXME: These specs fail with operation not permitted.
	rm -f spec/std/http/server/handlers/static_file_handler_spec.cr
	rm -f spec/std/time/location_spec.cr

	cat > Makefile.local <<-EOF
		progress = 1
		threads = ${JOBS:-2}
		verbose = 1
		FLAGS = --verbose --target $CTARGET ${BUILD_STATIC:+"--link-flags=-no-pie"}
		LLVM_CONFIG = $LLVM_CONFIG
	EOF
}

build() {
	make crystal \
		CRYSTAL_CONFIG_PATH="lib:$_shardsdir:$_coredir" \
		release=1 \
		static=${BUILD_STATIC:-} \
		interpreter=1
}

check() {
	#make std_spec threads=1 SPEC_FLAGS='--no-color --verbose'

	find samples -name '*.cr' -print0 | xargs -0 -tn 1 ./bin/crystal build --no-codegen

	# Takes ~1 hour on x86_64, ~1.5 hour on aarch64.
	#make compiler_spec threads=1 SPEC_FLAGS='--no-color --verbose'
}

package() {
	install -D -m 755 .build/crystal "$pkgdir"/usr/bin/crystal
	install -D -m 644 man/crystal.1 "$pkgdir"/usr/share/man/man1/crystal.1

	mkdir -p "$pkgdir$_coredir" "$pkgdir$_shardsdir"
	cp -r src/* "$pkgdir$_coredir"/

	install -D -m 644 etc/completion.bash \
		"$pkgdir"/usr/share/bash-completion/completions/$pkgname
	install -D -m 644 etc/completion.zsh \
		"$pkgdir"/usr/share/zsh/site-functions/_$pkgname

	#rm "$pkgdir$_coredir"/llvm/ext/llvm_ext.o
}

sha512sums=""
