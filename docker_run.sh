#!/bin/bash
eval $(ssh-agent)
docker run -d --rm -v $SSH_AUTH_SOCK -p 8888:8888 --name crystal-unified jkridner/crystal:latest /usr/bin/crystal play -p 8888 --binding 0.0.0.0
