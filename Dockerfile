FROM alpine:edge AS crystal-build
ENV CRYSTAL_CONFIG_VERSION=1.13.3
ENV CRYSTAL_CACHE_DIR="/tmp/.cache"
ENV LLVM_CONFIG="/usr/lib/llvm18/bin/llvm-config"
RUN echo '@edge http://dl-cdn.alpinelinux.org/alpine/edge/community' >>/etc/apk/repositories
RUN apk add --update --no-cache --force-overwrite \
	git \
	make \
	sudo \
	crystal@edge \
	build-base \
	gc-dev \
	gcc \
	gmp-dev \
	libatomic_ops \
	libevent-dev \
	libevent-static \
	musl-dev \
	pcre-dev \
	pcre2-dev \
	openssl-dev \
	openssl-libs-static \
	tzdata \
	xz-static \
	yaml-dev \
	yaml-static \
	zlib-dev \
	libxml2-dev \
	llvm18-dev \
	libxml2-static \
	llvm18-static \
	zlib-static \
	zstd-static \
	openssh
RUN adduser unified -D && \
  mkdir -p /etc/sudoers.d && \
  echo '%wheel ALL=(ALL) NOPASSWD:ALL' > /etc/sudoers.d/wheel && \
  adduser unified wheel && \
  adduser unified abuild
USER unified
WORKDIR /home/unified
COPY --chown=unified . /home/unified/crystal
RUN cd /home/unified/crystal && make crystal release=1 interpreter=1 PREFIX=/usr

FROM alpine:edge AS crystal
ENV LLVM_CONFIG="/usr/lib/llvm18/bin/llvm-config"
RUN echo '@edge http://dl-cdn.alpinelinux.org/alpine/edge/community' >>/etc/apk/repositories
RUN apk add --update --no-cache --force-overwrite \
	git \
	make \
	llvm18-dev \
	pcre-dev \
	libevent-dev \
	gc-dev \
	build-base \
	openssl-dev \
	openssh \
	sudo
RUN adduser unified -D && \
	mkdir -p /etc/sudoers.d && \
	echo '%wheel ALL=(ALL) NOPASSWD:ALL' > /etc/sudoers.d/wheel && \
	adduser unified wheel && \
	adduser unified abuild
COPY --from=crystal-build --chown=unified /home/unified/crystal /home/unified/crystal
WORKDIR /home/unified/crystal
RUN make install PREFIX=/usr
USER unified
WORKDIR /home/unified
