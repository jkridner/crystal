#!/bin/sh
cc -c -o src/llvm/ext/llvm_ext.o src/llvm/ext/llvm_ext.cc `llvm-config --cxxflags`
#`"/usr/bin/llvm-config-11" --libs --system-libs --ldflags --link-static 2> /dev/null`
cc -fuse-ld=lld \
	.build/crystal.o \
	src/llvm/ext/llvm_ext.o  \
	-o crystal \
	-rdynamic -static \
	-L/usr/lib/llvm-11/lib \
	-L/lib/aarch64-linux-musl \
	-lLLVMXRay -lLLVMWindowsManifest -lLLVMTableGen -lLLVMSymbolize -lLLVMDebugInfoPDB \
	-lLLVMPerfJITEvents -lLLVMOrcJIT -lLLVMOrcError -lLLVMJITLink -lLLVMObjectYAML \
	-lLLVMMCA -lLLVMLTO -lLLVMObjCARCOpts -lLLVMExtensions \
	-lLLVMPasses -lLLVMCoroutines -lLLVMLineEditor -lLLVMLibDriver -lLLVMInterpreter \
	-lLLVMFuzzMutate -lLLVMMCJIT -lLLVMExecutionEngine -lLLVMRuntimeDyld \
	-lLLVMDWARFLinker -lLLVMDlltoolDriver -lLLVMOption -lLLVMDebugInfoGSYM \
	-lLLVMCoverage -lLLVMXCoreDisassembler -lLLVMXCoreCodeGen \
	-lLLVMXCoreDesc -lLLVMXCoreInfo -lLLVMX86Disassembler -lLLVMX86CodeGen \
	-lLLVMX86AsmParser -lLLVMX86Desc -lLLVMX86Info \
	-lLLVMWebAssemblyDisassembler -lLLVMWebAssemblyCodeGen \
	-lLLVMWebAssemblyDesc -lLLVMWebAssemblyAsmParser -lLLVMWebAssemblyInfo \
	-lLLVMSystemZDisassembler -lLLVMSystemZCodeGen -lLLVMSystemZAsmParser \
	-lLLVMSystemZDesc -lLLVMSystemZInfo -lLLVMSparcDisassembler \
	-lLLVMSparcCodeGen -lLLVMSparcAsmParser -lLLVMSparcDesc -lLLVMSparcInfo \
	-lLLVMRISCVDisassembler -lLLVMRISCVCodeGen -lLLVMRISCVAsmParser \
	-lLLVMRISCVDesc -lLLVMRISCVUtils -lLLVMRISCVInfo \
	-lLLVMPowerPCDisassembler -lLLVMPowerPCCodeGen -lLLVMPowerPCAsmParser \
	-lLLVMPowerPCDesc -lLLVMPowerPCInfo -lLLVMNVPTXCodeGen -lLLVMNVPTXDesc \
	-lLLVMNVPTXInfo -lLLVMMSP430Disassembler -lLLVMMSP430CodeGen \
	-lLLVMMSP430AsmParser -lLLVMMSP430Desc -lLLVMMSP430Info \
	-lLLVMMipsDisassembler -lLLVMMipsCodeGen -lLLVMMipsAsmParser \
	-lLLVMMipsDesc -lLLVMMipsInfo -lLLVMLanaiDisassembler \
	-lLLVMLanaiCodeGen -lLLVMLanaiAsmParser -lLLVMLanaiDesc -lLLVMLanaiInfo \
	-lLLVMHexagonDisassembler -lLLVMHexagonCodeGen -lLLVMHexagonAsmParser \
	-lLLVMHexagonDesc -lLLVMHexagonInfo -lLLVMBPFDisassembler \
	-lLLVMBPFCodeGen -lLLVMBPFAsmParser -lLLVMBPFDesc -lLLVMBPFInfo \
	-lLLVMAVRDisassembler -lLLVMAVRCodeGen -lLLVMAVRAsmParser -lLLVMAVRDesc \
	-lLLVMAVRInfo -lLLVMARMDisassembler -lLLVMARMCodeGen -lLLVMARMAsmParser \
	-lLLVMARMDesc -lLLVMARMUtils -lLLVMARMInfo -lLLVMAMDGPUDisassembler \
	-lLLVMAMDGPUCodeGen -lLLVMMIRParser -lLLVMipo -lLLVMInstrumentation \
	-lLLVMVectorize -lLLVMLinker -lLLVMIRReader -lLLVMAsmParser \
	-lLLVMFrontendOpenMP -lLLVMAMDGPUAsmParser -lLLVMAMDGPUDesc \
	-lLLVMAMDGPUUtils -lLLVMAMDGPUInfo -lLLVMAArch64Disassembler \
	-lLLVMMCDisassembler -lLLVMAArch64CodeGen -lLLVMCFGuard \
	-lLLVMGlobalISel -lLLVMSelectionDAG -lLLVMAsmPrinter \
	-lLLVMDebugInfoDWARF -lLLVMCodeGen -lLLVMTarget -lLLVMScalarOpts \
	-lLLVMInstCombine -lLLVMAggressiveInstCombine -lLLVMTransformUtils \
	-lLLVMBitWriter -lLLVMAnalysis -lLLVMProfileData -lLLVMObject \
	-lLLVMTextAPI -lLLVMBitReader -lLLVMCore -lLLVMRemarks \
	-lLLVMBitstreamReader -lLLVMAArch64AsmParser -lLLVMMCParser \
	-lLLVMAArch64Desc -lLLVMMC -lLLVMDebugInfoCodeView -lLLVMDebugInfoMSF \
	-lLLVMBinaryFormat -lLLVMAArch64Utils -lLLVMAArch64Info -lLLVMSupport \
	-lLLVMDemangle \
	-lz -lrt -ldl -ltinfo -lpthread -lm \
	-lstdc++ \
	-lgc -levent -lffi \
       	-lpcre2-8 \
	#-lPolly -lPollyISL \

